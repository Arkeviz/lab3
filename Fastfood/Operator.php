<?php
namespace Fastfood;

require_once "Povar.php";
require_once "Customer.php";
use Fastfood\Povar as Povar;
use Fastfood\Customer as Customer;

class Operator extends Personal {
    public function getOrder($array, Povar $povar, Customer $customer){
        echo "{$this->post} {$this->name} получил(-a) заказ от {$customer->name} {$customer->surname}: ";
        foreach ($array as $i){
            echo "$i->name ";
        }
        echo "<br>{$this->post} {$this->name} передал(-a) заказ {$povar->post} {$povar->name}<br>";
        echo "{$povar->post} {$povar->name} получил(-a) заказ ";
        foreach ($array as $i){
            echo "$i->name ";
        }
        echo "<br>";
        $povar->makeOrder($array, $this);
    }
    public function giveFood($array){
        echo "{$this->post} {$this->name} передал(-a) заказ ";
        foreach ($array as $i){
            echo "$i->name ";
        }
        echo "покупателю<br>";
        echo "Покупатель говорит: \"Хорошая работа, ребята!\"<br>";
        echo "*Покупатель вкусно покушал*";
    }
}

?>