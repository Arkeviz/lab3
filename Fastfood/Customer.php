<?php
namespace Fastfood;

require_once "Operator.php";
require_once "Povar.php";
use Fastfood\Operator as Operator;
use Fastfood\Povar as Povar;

class Customer {
    public $name;
    public $surname;

    public function __construct($name, $surname)
    {
        $this->name=$name;
        $this->surname=$surname;
        echo "В зал прибыл {$this->name} {$this->surname}<br><br>";
    }

    public function orderFood(Operator $operator, $array, Povar $povar){
        $operator->getOrder($array, $povar, $this);
    }
}

?>