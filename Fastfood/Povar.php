<?php

namespace Fastfood;

require_once "Operator.php";
use Fastfood\Operator as Operator;

class Povar extends Personal {
    public function makeOrder($array, Operator $operator){
        echo "{$this->post} {$this->name} приготовил(-a) ";
        foreach ($array as $i){
            echo "$i->name ";
        }
        echo " и торжественно произнес: \"Делсделано\"<br>";
        echo "{$this->post} {$this->name} передал заказ {$operator->post} {$operator->name}<br>";
        $operator->giveFood($array);
    }
}

?>