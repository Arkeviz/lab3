<?php
namespace Interfaces;

interface CanDeliver {
    public function deliver():string;
}

?>