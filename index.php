<?php

require_once "Transport\Package.php";
require_once "Transport\Car.php";
require_once "Transport\Plane.php";
require_once "Transport\Train.php";
use Transport\Package as Package;
use Transport\Car as Car;
use Transport\Plane as Plane;
use Transport\Train as Train;

// Задание 1
echo "<h2>Задание 1</h2>";
$apples = new Package(100,12345);
$trainKotorySmog = new Train("Поезд", "Thomas");
echo $trainKotorySmog->deliver();
echo $trainKotorySmog->addPackage($apples);
echo $trainKotorySmog->addPackage($apples);
echo $trainKotorySmog->deliver();

echo "<br>Тем временем в другом депо... <br><br>";
$bananas = new Package(700,38147);
$plane = new Plane("Самолёт", "Muhammad");
echo $plane->deliver();
echo $plane->addPackage($bananas);
echo $plane->addPackage($bananas);

echo "<br>";
$kapybara = new Package(1700,48959);
$lightningMcQueen = new Car("Машина", "Monty");
echo $lightningMcQueen->deliver();
echo $lightningMcQueen->addPackage($kapybara);
echo $lightningMcQueen->addPackage($kapybara);

echo "<br>";
echo $lightningMcQueen->getStoredPackageNumber();
echo $plane->getStoredPackageNumber();
echo $lightningMcQueen->transportPackageSwap($plane);
echo "<br>";
echo $lightningMcQueen->getStoredPackageNumber();
echo $plane->getStoredPackageNumber();
echo "<br>";
echo $lightningMcQueen->deliver();
echo "<br>";
echo $plane->deliver();
echo "<br>";

echo "<hr size=\"20\" color=\"#8922FF\">";

// Задание 2

echo "<h2>Задание 2</h2>";

require_once "doggoFamily\GrandDoggo.php";
use doggoFamily\GrandDoggo as GrandDoggo;
require_once "doggoFamily\PapaDoggo.php";
use doggoFamily\PapaDoggo as PapaDoggo;
require_once "doggoFamily\Doggo.php";
use doggoFamily\Doggo as Doggo;

$doggoGena = new GrandDoggo("Альзур Северный Ветер", "Гена", 23.4);
$doggoGena->getDoggoInfo();
$doggoGena->doggoIntroduse();
echo "<br>";
$doggoMaya = new PapaDoggo("Майя Северный Ветер", "Майя", 18.2, false, "Медвежья");
$doggoMaya->getDoggoInfo();
$doggoMaya->doggoIntroduse();
echo "<br>";
$doggoSnejok = new Doggo("Ульрик Северный Ветер", "Снежок", 13.7, true, "Волчья", true);
$doggoSnejok->getDoggoInfo();
$doggoSnejok->doggoIntroduse();
echo "<br>";

echo "<hr size=\"20\" color=\"#8922FF\">";

// Задание 3

echo "<h2>Задание 3</h2>";

require_once "Fastfood\Personal.php";
require_once "Fastfood\Operator.php";
require_once "Fastfood\Povar.php";
require_once "Fastfood\Food.php";
require_once "Fastfood\Customer.php";
use Fastfood\Personal as Personal;
use Fastfood\Operator as Operator;
use Fastfood\Povar as Povar;
use Fastfood\Food as Food;
use Fastfood\Customer as Customer;

$burgir = new Food("Бургер", 63);
$brotato = new Food("Картошка", 120);
$cofii = new Food("Кофе", 160);
$array = [$burgir, $brotato];
$operator = new Operator("Чебурашка", "Игоревич", "Оператор");
$povar = new Povar("Гена", "Артемьевич", "Повар");
$customer1 = new Customer("Михаил", "Зубенко");
$customer1->orderFood($operator, $array, $povar);