<?php
namespace Transport;

class Package {
    public $weight;
    public $number;

    public function __construct($weight, $number)
    {
        $this->weight = $weight;
        $this->number = $number;
    }
}

?>