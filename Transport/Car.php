<?php
namespace Transport;
 
require_once "Interfaces\CanDeliver.php";
require_once "Transport.php";
use Interfaces\CanDeliver as CanDeliver;
use Transport\Transport as Transport;

class Car extends Transport implements CanDeliver {

    public function deliverySound()
    {
        echo "*Мип-мип*<br>";
    }
}

?>