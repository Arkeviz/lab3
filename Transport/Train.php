<?php
namespace Transport;

require_once "Interfaces\CanDeliver.php";
require_once "Transport.php";
use Interfaces\CanDeliver as CanDeliver;
use Transport\Transport as Transport;

class Train extends Transport implements CanDeliver {

    public function deliverySound()
    {
        echo "*Чага-чага чага-чага тю-тю*<br>";
    }
}

?>