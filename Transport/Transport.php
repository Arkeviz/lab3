<?php
namespace Transport;

require_once "Package.php";  
require_once "Interfaces\CanDeliver.php";
use Transport\Package as Package;
use Interfaces\CanDeliver as CanDeliver;

class Transport implements CanDeliver {
    public $trType;
    public $trName;
    public $storedPackage;

    public function __construct($trType, $trName, $storedPackage = null)
    {
        $this->trType=$trType;
        $this->trName=$trName;
        $this->storedPackage=$storedPackage;
        echo "{$this->trType} {$this->trName} был(-а) призван(-а) и готов(-а) к службе!<br>";
    }

    public function deliverySound()
    {
        echo "*wee-wee*<br>";
    }

    public function addPackage(Package $package):string 
    {
        if (!$this->storedPackage == null) {
           return "Этот(-та) {$this->trType} {$this->trName} уже забит(-а), сорян<br>";
        }
        $this->storedPackage = $package;
        return "Мы добавили посылку {$package->number} и готовы отпрвляться!<br>";
    }

    public function getStoredPackageNumber() {
        echo "Номер груза у {$this->trName}:{$this->storedPackage->number}<br>";
    }
    

    public function deliver():string
    {
        if ($this->storedPackage == null){
            return "Пока {$this->trName} нечего досавлять<br>";
        }
        $this->deliverySound();
        return "{$this->trType} {$this->trName} доставил(-а) посылку {$this->storedPackage->number}<br>";
    }

    public function transportPackageSwap(Transport $transport):string 
    {
        if($this->storedPackage == null && $transport->storedPackage == null)
        {
            return "{$this->trName} и {$transport->trName} нечем обмениваться :(<br>";
        }

        else if ($this->storedPackage != null && $transport->storedPackage != null) 
        {
            $tmp = $this->storedPackage;
            $this->storedPackage = $transport->storedPackage;
            $transport->storedPackage = $tmp;
            return "{$this->trName} обменялся грузами с {$transport->trName} ({$transport->storedPackage->number} на {$this->storedPackage->number})<br>";
        }

        else if ($transport->storedPackage == null) 
        {
            $transport->storedPackage = $this->storedPackage;
            return "{$this->trName} передал груз {$transport->trName}<br>";
        }

        $this->storedPackage = $transport->storedPackage;
        return "{$this->trName} получил груз от {$transport->trName}<br>";
    }
}

?>