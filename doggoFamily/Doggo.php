<?php

namespace doggoFamily;

class Doggo extends PapaDoggo
{    
    public $eyeSack; // есть - true, нет - false

    public function __construct($fullName, $name, $weight, $gender, $headForm, $eyeSack)
    {
        $this->fullName=$fullName;
        $this->name=$name;
        $this->weight=$weight;
        $this->gender=$gender;
        $this->headForm=$headForm;
        $this->eyeSack=$eyeSack;
        echo "Собакен появился! ВУФ-ВУФ<br><br>";
    }

    public function doggoIntroduse() {
        echo "*{$this->name} ОЧЕНЬ ГРОМКО ВУФКАЕТ*<br>";
    }

    public function getDoggoInfo() {
        parent::getDoggoInfo();
        switch($this->eyeSack){
            case (true):
                echo "Подглазники: Есть<br>";
                break;
            case (false):
                echo "Подглазники: Нет<br>";
                break;
            default:
                echo "Подглазники: Не указан<br>";
        }
    }
}