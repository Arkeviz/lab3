<?php

namespace doggoFamily;

class PapaDoggo extends GrandDoggo
{    
    public $headForm;

    public function __construct($fullName, $name, $weight, $gender, $headForm)
    {
        $this->fullName=$fullName;
        $this->name=$name;
        $this->weight=$weight;
        $this->gender=$gender;
        $this->headForm=$headForm;
        echo "Собакен появился! Вуф-вуф<br><br>";
    }

    public function doggoIntroduse() {
        echo "*{$this->name} умеренно вуфкаеф*<br>";
    }

    public function getDoggoInfo() {
        parent::getDoggoInfo();
        echo "Форма головы: {$this->headForm}<br>";
    }
}