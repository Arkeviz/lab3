<?php

namespace doggoFamily;

class GrandDoggo
{    
    public $fullName;
    public $name;
    public $weight;
    public $gender; // муж. - true, жен. - false

    public function __construct($fullName, $name, $weight, $gender = null)
    {
        $this->fullName=$fullName;
        $this->name=$name;
        $this->weight=$weight;
        $this->gender=$gender;
        echo "Собакен появился! Вуф<br><br>";
    }

    public function doggoIntroduse() {
        echo "*{$this->name} устало виляет хвостом*<br>";
    }

    public function getDoggoInfo() {
        echo "
        Собакен породы Самоед.<br>
        Полное имя: {$this->fullName}<br>
        Кличка: {$this->name}<br>
        Вес: {$this->weight}<br>";

        if (!isset($this->gender)) {
            echo 'Пол: Не задан<br>';
        } 
        else {
            echo ($this->gender) ? 'Пол: Муж.<br>' : 'Пол: Жен.<br>';
        }
    }
}